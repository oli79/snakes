
# determine if 32bit or 64bit architecture
# := is expanded when defined rather when used
# value is either "32" or "64"
ARCH := $(shell getconf LONG_BIT)

# get branch name and commit count (to build version string with macros)
COMMITCNT 	:= $(shell git rev-list --count HEAD)
BRANCH 		:= $(shell git symbolic-ref --short HEAD)

CC      = gcc
# -D = define constant/macro for preprocessor
# -g for debugger
# -g and -pg for gprof (binary poops out gmon.out at every invocation, so i am not going to leave it on permanently)
CFLAGS_COMMON   = -Wall -Wextra -Wpedantic -Werror -D ARCH_$(ARCH)BIT -D COMMITCNT=\"$(COMMITCNT)\" -D BRANCH=\"$(BRANCH)\"
CFLAGS		= -g $(CFLAGS_COMMON) -D DEBUG
CFLAGS_REL   	= -O3 $(CFLAGS_COMMON) -D RELEASE

SRC     = ./src
BIN     = ./bin

TARGETS 	= snakes
TARGETS_BIN 	= $(patsubst %,$(BIN)/%,$(TARGETS))

define msg
	@echo "\n$1 \"$2\" with dependencies \"$^\""
endef
#define msg
#	@echo "\n\n\n******************************************************************************"
#	@echo "* $1:$2"
#	@echo "*  dependencies: $^"
#endef


#.PHONY means these rules get executed even if files of those names exist.
.PHONY: test clearscreen

all: $(TARGETS_BIN)

release: prep_release all

levels: 
	@echo "downloading levels from https://github.com/alexdantas/nSnake"
	mkdir levels
	git clone https://github.com/alexdantas/nSnake levels/nSnake
	cp levels/nSnake/levels/* levels
	rm -rf levels/nSnake

prep_release: clean
	@echo "release build"
	$(eval CFLAGS := $(CFLAGS_REL))


clearscreen:
	clear

clean:
	rm -f $(BIN)/* core.*
	@echo "\n"

test:
	@echo "\n folders:"
	@echo $(SRC)
	@echo $(BIN)
	@echo "\n targets:"
	@echo $(TARGETS)
	@echo $(TARGETS_BIN)

# **************************************************************************************************** 
# targets
# **************************************************************************************************** 

# compiling "bin/file" depending on "src/file.c"
$(TARGETS_BIN): $(BIN)/%: $(SRC)/%.c
	$(call msg,compiling to $(ARCH)bit architecture,$@)
	$(CC) $(CFLAGS) $< -o $@


