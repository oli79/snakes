/*

   snakes, minimalistic terminal game

   Copyright 2024, Oliver Isler

   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

   Project website is at https://www.gitlab.com/oli79/snakes

*/

#define COPYRIGHT1 "This program comes with ABSOLUTELY NO WARRANTY. This is free software,"
#define COPYRIGHT2 "and you are welcome to redistribute it under certain conditions."
#define COPYRIGHT3 "See <https://www.gnu.org/licenses/>. For contact details, source code, "
#define COPYRIGHT4 "documentation, instructions and bug reports see the project web site at "
#define COPYRIGHT5 "<https://gitlab.com/oli79/snakes>."
#define COPYRIGHT COPYRIGHT1 " " COPYRIGHT2 " " COPYRIGHT3 " " COPYRIGHT4 " " COPYRIGHT5

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <argp.h>
#include <argz.h>
#include <limits.h> // PATH_MAX
#include <regex.h>

// program version macro
//********************************************************************************

#if defined(COMMITCNT) && defined(BRANCH)
#define VERSION "v." BRANCH ".0." COMMITCNT
#else
#define VERSION "version <unknown>"
#endif


// compiler options
//********************************************************************************

// architecture
#ifdef ARCH_32BIT
#define ARCHITECTURE "32bit"
#elif ARCH_64BIT
#define ARCHITECTURE "64bit"
#else 
#define ARCHITECTURE "<unknown>"
#endif

// build
#ifdef DEBUG
#define BUILD "DEBUG"
#elif RELEASE
#define BUILD "RELEASE"
#else 
#define BUILD "<unknown>"
#endif


// ansi escape sequences
//********************************************************************************

#define ESC			"\x1b["
#define HIDE_CURSOR   	ESC"?25l"
#define SHOW_CURSOR   	ESC"?25h"
#define CUR(x, y)  	printf(ESC"%d;%dH", (y), (x)+game->xoff)
// cursor positioning with wide mode as option
// 0/0 is top left of border
#define CUR2(x, y, f) 	printf(ESC"%d;%dH", (y)+game->yoff, ((x)*(f))+game->xoff)

#define RESET        "\x1b[0m"
#define BOLD         "\x1b[1m"

#define BLACK        "\x1b[30m"
#define RED          "\x1b[31m"
#define GREEN        "\x1b[32m"
#define YELLOW       "\x1b[33m"
#define BLUE         "\x1b[34m"
#define MAGENTA      "\x1b[35m"
#define CYAN         "\x1b[36m"
#define WHITE        "\x1b[37m"

#define BGBLACK      "\x1b[40m"
#define BGRED        "\x1b[41m"
#define BGGREEN      "\x1b[42m"
#define BGYELLOW     "\x1b[43m"
#define BGBLUE       "\x1b[44m"
#define BGMAGENTA    "\x1b[45m"
#define BGCYAN       "\x1b[46m"
#define BGWHITE      "\x1b[47m"


// macros
//********************************************************************************

#define MIN(x,y) ((x)<(y)?(x):(y))
#define MAX(x,y) ((x)<(y)?(y):(x))

// if !(cond), print to stderr and assert(cond)
#define assert_err(cond, ...) if (!(cond)) {reset_terminal_mode(); fprintf(stderr, "%s:%i: %s: ", __FILE__, __LINE__, __func__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); assert(cond);}
//#define assert_err(cond, ...) if (!(cond)) {int assert_ptr=0; assert_ptr+=sprintf(error_msg, "%s:%i: %s: ", __FILE__, __LINE__, __func__); assert_ptr+=sprintf(error_msg+assert_ptr, __VA_ARGS__); assert_ptr+=sprintf(error_msg+assert_ptr, "\n"); assert(cond);}

// turn a macro constant integer into a string of the integer
#define STR2(a) #a
#define STR(a) STR2(a)

// game
// sleep in ms
#define GAME_INIT_SLEEP 250
// speed factor in %
// delay = delay * 100 / (100 + SPEED_FACTOR)
#define SPEED_FACTOR  3
#define SPEED_DIVIDEND 100
#define TITLE "snakes " VERSION " Copyright (C) Oliver Isler 2024, GPLv3"
// help text (HT)
#define LEVEL_HT "[LEVEL_MAP(s)...]"
#define TITLE_HT "\n" TITLE "\n\nArguments:\n  " LEVEL_HT "          nSnake level file(s), e.g. \"path/*.nsnake"
#define FOOD_QUANTITY     1
#define OBSTACLES_QUANTITY  0
#define PORTALS_QUANTITY  0
#define LEVEL_PATH "./levels"
#define PORTAL_CNT_MAX 1

// functions
//#define RAND(a) ((unsigned int) rand()) % a

// clear screen with ansi code
#define clear {printf(ESC "2J");}

// screen
// #define MAX_X ((unsigned int) 50)
// #define MAX_Y ((unsigned int) 20)
#define SPACING 1

// snake consts
#define SNAKE_BODY_MAX 500
#define START_SNAKE_LENGTH 3
#define START_SNAKE_GROWING 3
#define GROWRATE 5

// level map file
// tag to denote start of map
#define LEVEL_START "start"
// tag to denote end of map
#define LEVEL_END "end"
// tag to denote player starting position
#define LEVEL_PLAYER '@'

// structs
//********************************************************************************

struct st_options {
	bool skiptitle;
	unsigned int speed;
	bool debug;
	unsigned int portals;
	unsigned int food;
	unsigned int obstacles;
	unsigned int growrate;
	unsigned int speedfactor;
	bool widemode;
	bool unicorn;
	bool yummy;
	// args (level map files, w/o options)
	char* argz;
	size_t argz_len;
	size_t argc;
};

enum e_direction { Stop, Up, Down, Left, Right };

struct termios orig_termios;

struct st_pos {
	unsigned int x;
	unsigned int y;
};

struct st_char {
	char *c;
	char *color;
};

struct st_obstacles {
	struct st_pos p;
	char *c;
};

struct st_snake {
	struct st_pos body[SNAKE_BODY_MAX];
	unsigned int head;
	unsigned int tail;
	struct st_char c;
	unsigned int growing;
	unsigned int length;
	enum e_direction dir;
	enum e_direction new_dir;
};

// magic number Food
#define Food 200
#define Portals 100
enum e_cmap_cell {BadCell, Empty, Border, Obstacle, Snake, Wall /*Portals & Food magic number*/};
struct st_cmap {
	enum e_cmap_cell cell;
};

#define SNAKE_CHAR "█"
#define WIDE_SNAKE_CHAR "██"

char *food_chars[] = { "*", "%", "$", "#", "&", "@", ".", ",", "+" };
char *wide_food[] = { "**", "%%", "$$", "##", "&&", "@@", "..", ",,", "++" };

// yummy food 🧁 🍦 🍨 🍟 🍔 🍓 🍪 🍬 🍫 🍩 🍧 🍿
char *yummy_food[] = { "\U0001F9C1", "\U0001F366", "\U0001F368", "\U0001F35F", "\U0001F354", "\U0001F353", "\U0001F36A", "\U0001F36C", "\U0001F36B", "\U0001F369", "\U0001F367", "\U0001F37F"};

// yucky food 🫑 🥦 🥕 🥗 🥬 🫒 🪲 🕷 🥒 🦟
char *yucky_food[] = {"\U0001FAD1", "\U0001F966", "\U0001F955", "\U0001F957", "\U0001F96C", "\U0001FAD2", "\U0001FAB2", "\U0001F577", "\U0001F952", "\U0001F99F"};

// 💓 Beating Heart 💕 Two Hearts 💖 Sparkling Heart 💗 Growing Heart 💘 Heart with Arrow 💝 Heart with Ribbon 💞 Revolving Hearts
char *hearts[] = { "\U0001F493", "\U0001F495", "\U0001F496", "\U0001F497", "\U0001F498", "\U0001F49D", "\U0001F49E"};


struct st_portal {
	struct st_pos p;
	struct st_char c;
};

struct st_food {
	struct st_pos p;
	struct st_char c;
};

struct st_terminal {
	unsigned int lines;
	unsigned int columns;
};

struct st_level_map {
	char **file;
	char *file_name;
	unsigned int index;
	unsigned int file_line_cnt;
	unsigned int start_line;
	unsigned int end_line;
	unsigned int file_array_len;
	unsigned int xmax;
	unsigned int ymax;
	struct st_pos player_starting_position;
};

enum e_change_map { None, Next, Previous };

enum e_game_state {Init, Title, Pause, Credits, Help, Game, Death, Win, Quit};

struct st_game {
	// cli options
	struct st_options *cli_options;
	bool widemode;
	bool unicorn;
	bool yummy;
	bool skiptitle;
	unsigned int portals_quantity;
	unsigned int food_quantity;
	unsigned int obstacle_quantity;
	unsigned int growrate;
	unsigned int speedfactor;
	bool debug;

	// terminal
	struct st_terminal terminal;
	unsigned int xoff;
	unsigned int yoff;
	// new dimensions for new game
	// in case dimensions of terminal have changed in the mean time
	unsigned int ylen;
	unsigned int xlen;
	// old dimensions for old game (to free up memory)
	// in case dimensions of terminal have changed in the mean time
	unsigned int xlen_old;
	unsigned int ylen_old;
	unsigned int xfact; // wide ? 2 : 1;

	// agents
	struct st_cmap **cmap;
	struct st_snake *snake;
	struct st_portal *portals;
	struct st_food *food;
	struct st_obstacles *obstacles;
	struct st_level_map level_map;
	char *obstacle_color;
	char *border_color;

	// game state
	unsigned int portals_cnt;
	unsigned long int cnt;
	unsigned long int yay;
	enum e_game_state state;
	enum e_game_state new_state;
	enum e_change_map change_map;
	long int sleep; // ms
	bool random_map;
	bool redraw_game;
	bool running;
	bool clear_screen;
	bool clear_status;
	bool zero_speed;
};


// functions
//********************************************************************************

// add to list (array)
// list grows dynamically if new memory is required
void **enlist(void **list, void *element, unsigned long int index, unsigned int *arraylength, int initsize, float resizefactor) {
	// initsize array
	if (list == NULL) {
		list = malloc(sizeof(void *) * initsize);
		*arraylength = initsize;
		// init pointers
		for (unsigned long int i=0; i < *arraylength; i++)
			list[i] = NULL;
	}
	// reallocate memory, increase array by factor
	if ((*arraylength)<=index) {
		unsigned int oldindex = *arraylength;
		(*arraylength) = (unsigned int) (*arraylength) * resizefactor;
		list = realloc(list, sizeof(void *) * (*arraylength));
		// init pointers
		for (unsigned int i = oldindex; i < *arraylength; i++) {
			list[i] = NULL;
		}
	}
	// store data in list
	list[index] = element;
	return list;
}


// reset terminal mode (use settings from backup)
void reset_terminal_mode() {
	tcsetattr(0, TCSANOW, &orig_termios);
}

void exit_handler() {
	reset_terminal_mode();
	printf(ESC"2J" ESC"H" BOLD GREEN "thank you for playing, have a nice day :)\n\n"); // clear screen, go 0/0
	printf(SHOW_CURSOR);
}

void set_conio_terminal_mode() {
	struct termios new_termios;

	/* take two copies - one for now, one for later */
	tcgetattr(0, &orig_termios);
	memcpy(&new_termios, &orig_termios, sizeof(new_termios));

	/* register cleanup handler, and set the new terminal mode */
	atexit(exit_handler);
	cfmakeraw(&new_termios);
	tcsetattr(0, TCSANOW, &new_termios);
}

char *randomFoodCharacter(struct st_game *game) {
	// return yummy food
	if (game->yummy) {
		int i = rand() % (sizeof(yummy_food)/sizeof(yummy_food[0]));
		// ***********
		return yummy_food[i];
		// ***********
	}
	// return wide food
	else if (game->widemode) {
		int i = rand() % (sizeof(wide_food)/sizeof(wide_food[0]));
		// ***********
		return wide_food[i];
		// ***********
	}
	// return single char food
	int i = rand() % (sizeof(food_chars)/sizeof(food_chars[0]));
	// ***********
	return food_chars[i];
	// ***********
}

char *randomObstacleCharacter(struct st_game *game) {
	// return yucky
	if (game->unicorn) {
		int i = rand() % (sizeof(yucky_food)/sizeof(yucky_food[0]));
		// ***********
		return yucky_food[i];
		// ***********
	}
	// return wide char
	else if (game->widemode) {
		// ***********
		return WIDE_SNAKE_CHAR;
		// ***********
	}
	// return single char
	// ***********
	return SNAKE_CHAR;
	// ***********
}

#define color(a) case a: s = "\x1b[3" #a "m";

#define BLACK        "\x1b[30m"
#define RED          "\x1b[31m"
#define GREEN        "\x1b[32m"
#define YELLOW       "\x1b[33m"
#define BLUE         "\x1b[34m"
#define MAGENTA      "\x1b[35m"
#define CYAN         "\x1b[36m"
#define WHITE        "\x1b[37m"

char *randomColor() {
	char *s = NULL;
	// skip 0 (black)
	switch (rand() % 6 + 1) {
		// macro color(int)
		color(0)
			break;
		color(1)
			break;
		color(2)
			break;
		color(3)
			break;
		color(4)
			break;
		color(5)
			break;
		color(6)
			break;
		color(7)
			break;

	}
	return s;
}

void freeCollisionMap(struct st_game *game) {
	if (game->cmap == NULL) return;

	for (unsigned int i=0; i < game->xlen_old; i++) {
		free(game->cmap[i]);
	}
	free(game->cmap);
}

void initCollisionMap(struct st_game *game) {

	// free if old collision map exists
	freeCollisionMap(game);

	// make new collision map
	game->cmap = malloc(sizeof(struct st_cmap *) * (game->xlen));
	for (unsigned int i=0; i < game->xlen; i++) {
		game->cmap[i] = malloc(sizeof(struct st_cmap) * (game->ylen));
	}

	// initialize collision map
	for (unsigned int x=0; x < game->xlen; x++) {
		for (unsigned int y=0; y < game->ylen; y++) {
			game->cmap[x][y].cell = Empty;
		}
	}
}

// true = out of bounds
// false = not ouf bounds
bool checkOutOfBounds(struct st_game *game, struct st_pos *p) {
	bool out_of_bounds = false;
	if (p->x >= game->xlen) out_of_bounds = true; 
	if (p->y >= game->ylen) out_of_bounds = true; 
	if (p->x <= 0) out_of_bounds = true; 
	if (p->y <= 0) out_of_bounds = true; 
	return out_of_bounds;
}

bool checkCollisionMap(struct st_game *game, struct st_pos *p) {
#ifdef DEBUG
	assert_err(!checkOutOfBounds(game, p), "p %d %d off %d %d len %d %d\n", p->x, p->y, game->xoff, game->yoff, game->xlen, game->ylen);
#else
	if (checkOutOfBounds(game, p)) return true;
#endif
	// not empty --> true --> collision
	// empty --> false --> no collision
	return (game->cmap[p->x][p->y].cell != Empty);
}

// get cell at position from collision map
enum e_cmap_cell getCollisionMapCell(struct st_game *game, struct st_pos *p) {
#ifdef DEBUG
	assert_err(!checkOutOfBounds(game, p), "p %d %d off %d %d len %d %d\n", p->x, p->y, game->xoff, game->yoff, game->xlen, game->ylen);
#else
	if (checkOutOfBounds(game, p)) return BadCell;
#endif
	return game->cmap[p->x][p->y].cell;
}

// set cell at position in collision map
void setCollisionMapCell(struct st_game *game, struct st_pos *p, enum e_cmap_cell c) {
#ifdef DEBUG
	assert_err(!checkOutOfBounds(game, p), "p %d %d off %d %d len %d %d\n", p->x, p->y, game->xoff, game->yoff, game->xlen, game->ylen);
#else
	if (checkOutOfBounds(game, p)) return;
#endif
	game->cmap[p->x][p->y].cell = c;
}

// get empty random position and mark it, return position
struct st_pos putOnCollisionMapAtRandomPosition(struct st_game *game, enum e_cmap_cell c) {
	struct st_pos p;
	do {
		p.x = (unsigned int)(rand() % (game->xlen-1) + 1); // +1 = outside left / top border
		p.y = (unsigned int)(rand() % (game->ylen-1) + 1);
		// make sure position is not taken yet
	} while (checkCollisionMap(game, &p));
	setCollisionMapCell(game, &p, c);
	return p;
}

char *randomPortalColor() {
	char *s = NULL;
	switch (rand() % 3) {
		case 0:
			s = BGBLUE WHITE;
			break;
		case 1:
			s = BGMAGENTA BLACK;
			break;
		case 2:
			s = BGYELLOW RED;
			break;
	}
	return s;
}


void initPortals(struct st_game *game, int index) {
	// init all
	if (index < 0) {
		// create if null
		if (game->portals == NULL) {
			game->portals = malloc(sizeof(struct st_portal) * game->portals_quantity);
		} 
		// erase portals on screen
		else {
			for (unsigned int i=0; i<game->portals_quantity; i++) {
				CUR2(game->portals[i].p.x, game->portals[i].p.y, game->xfact);
				printf(" ");
				if (game->widemode) 
					printf("  ");
				else
					printf(" ");
			}
		}
		// make all new portals
		for (unsigned int i=0; i<game->portals_quantity; i++) {
			game->portals[i].p = putOnCollisionMapAtRandomPosition(game, Portals+i);
			if (game->widemode)
				game->portals[i].c.c = "[]";
			else
				game->portals[i].c.c = "O";
			game->portals[i].c.color = randomPortalColor();
		}
	}
	// make new portals at index
	else {
		// erase portals on screen
		CUR2(game->portals[index].p.x, game->portals[index].p.y, game->xfact);
		if (game->widemode) 
			printf("  ");
		else
			printf(" ");

		// get new random position
		game->portals[index].p = putOnCollisionMapAtRandomPosition(game, Portals+index);
		if (game->widemode)
			game->portals[index].c.c = "[]";
		else
			game->portals[index].c.c = "O";
		game->portals[index].c.color = randomPortalColor();

		// draw the new portals
		CUR2(game->portals[index].p.x, game->portals[index].p.y, game->xfact);
		printf("%s%s", game->portals[index].c.color, game->portals[index].c.c);
	}
	// clear background color
	printf(RESET);
}


void initFood(struct st_game *game, int index) {
	// init all
	if (index < 0) {
		// create if null
		if (game->food == NULL) {
			game->food = malloc(sizeof(struct st_food) * game->food_quantity);
		} 
		// erase food on screen
		else {
			for (unsigned int i=0; i<game->food_quantity; i++) {
				CUR2(game->food[i].p.x, game->food[i].p.y, game->xfact);
				printf(" ");
				if (game->widemode) 
					printf("  ");
				else
					printf(" ");
			}
		}
		// make all new food
		for (unsigned int i=0; i<game->food_quantity; i++) {
			game->food[i].p = putOnCollisionMapAtRandomPosition(game, Food+i);
			game->food[i].c.c = randomFoodCharacter(game);
			game->food[i].c.color = randomColor();
		}
	}
	// make new food at index
	else {
		// erase food on screen
		CUR2(game->food[index].p.x, game->food[index].p.y, game->xfact);
		if (game->widemode) 
			printf("  ");
		else
			printf(" ");

		// get new random position
		game->food[index].p = putOnCollisionMapAtRandomPosition(game, Food+index);
		game->food[index].c.c = randomFoodCharacter(game);
		game->food[index].c.color = randomColor();

		// draw the new food
		CUR2(game->food[index].p.x, game->food[index].p.y, game->xfact);
		printf("%s%s", game->food[index].c.color, game->food[index].c.c);
	}
}


void initSnake(struct st_game *game) {
	if (game->snake != NULL)
		free(game->snake);
	struct st_snake *snake = malloc(sizeof(struct st_snake));

	// init array
	for (unsigned int i=0; i<SNAKE_BODY_MAX; i++) {
		snake->body[i].x = 0;
		snake->body[i].y = 0;
	}

	// place the snake
	snake->tail = 0;
	snake->head = 1;
	// if no map or starting position out of bounds, take default starting position (e.g. random position)
	if ((game->level_map.file == NULL) || (checkOutOfBounds(game, &game->level_map.player_starting_position))) {
		struct st_pos p;
		bool position_valid;
		unsigned int attempts = 0;
		do {
			attempts++;
			assert_err((attempts<(game->xlen*game->ylen*10)), "failed to find a valid stop for snake head");
			p.x = 0;
			p.y = 0;
			do {
				unsigned int spacing = 3;
				if (game->xlen <= spacing * 2) {
					p.x++;
					p.x = p.x % game->xlen;
				}
				else {
					p.x = (unsigned int)(rand() % (game->xlen-spacing * 2) + spacing);
				}
				if (game->ylen <= spacing * 2) {
					p.y++;
					p.y = p.y % game->ylen;
				}
				else {
					p.y = (unsigned int)(rand() % (game->ylen-spacing * 2) + spacing);
				}
				// make sure position is not taken yet
			} while (checkCollisionMap(game, &p));

			// check if position up/down/left/right of snake head free
			position_valid = false;
			struct st_pos u = p;
			u.y--;
			struct st_pos d = p;
			d.y++;
			struct st_pos l = p;
			l.x--;
			struct st_pos r = p;
			r.x++;
			// left half
			if (game->xlen/2 > p.x) {
				if (!checkCollisionMap(game, &r)) {
					snake->dir = Right;
					snake->new_dir = Right;
					position_valid = true;
				}
				else if (!checkCollisionMap(game, &l)) {
					snake->dir = Left;
					snake->new_dir = Left;
					position_valid = true;
				}
			} 
			// right half
			else {
				if (!checkCollisionMap(game, &l)) {
					snake->dir = Left;
					snake->new_dir = Left;
					position_valid = true;
				}
				else if (!checkCollisionMap(game, &r)) {
					snake->dir = Right;
					snake->new_dir = Right;
					position_valid = true;
				}
			}
			if (!position_valid) {
				// upper half
				if (game->ylen/2 > p.y) {
					if (!checkCollisionMap(game, &d)) {
						snake->dir = Down;
						snake->new_dir = Down;
						position_valid = true;
					}
					else if (!checkCollisionMap(game, &u)) {
						snake->dir = Up;
						snake->new_dir = Up;
						position_valid = true;
					}
				}
				// lower half
				else {
					if (!checkCollisionMap(game, &u)) {
						snake->dir = Up;
						snake->new_dir = Up;
						position_valid = true;
					}
					else if (!checkCollisionMap(game, &d)) {
						snake->dir = Down;
						snake->new_dir = Down;
						position_valid = true;
					}
				}
			}
		} while (!position_valid);

		if (snake->dir == Left) {
			snake->body[snake->tail].x = p.x+1;
		}
		else {
			snake->body[snake->tail].x = p.x-1;
		}
		snake->body[snake->tail].y = p.y;
		snake->body[snake->head].x = p.x;
		snake->body[snake->head].y = p.y;
		//assert_err(0, "dir %i\n", snake->dir);
	} 
	// if there is a map, take its starting position for the snake
	else {
		snake->tail = 0;
		snake->head = 1;
		unsigned int x = game->level_map.player_starting_position.x + 1;
		unsigned int y = game->level_map.player_starting_position.y + 1;
		snake->body[snake->tail].x = x-1;
		snake->body[snake->tail].y = y;
		snake->body[snake->head].x = x;
		snake->body[snake->head].y = y;
		snake->dir = Right;
		snake->new_dir = Right;
	}

	snake->length = snake->head-snake->tail+1;
	snake->growing = START_SNAKE_GROWING;

	// write snake body into the collision map
	// "tail" is actually not the tail but the first "empty" tile after the tail --> i=1
	for (unsigned int i=1; i<snake->length; i++) {
		setCollisionMapCell(game, &snake->body[(snake->tail+i) % SNAKE_BODY_MAX], Snake);
	}

	// unicorn mode
	if (game->unicorn) {
		snake->c.c = "\U0001F984";
	}
	// double char mode
	else if (game->widemode) {
		snake->c.c = WIDE_SNAKE_CHAR;
	}
	// single char mode
	else {
		snake->c.c = SNAKE_CHAR;
	}
	snake->c.color = randomColor();

	game->snake = snake;
}

void initObstacles(struct st_game *game) {
	if (game->obstacles == NULL) {
		game->obstacles = malloc(sizeof(struct st_obstacles) * game->obstacle_quantity);
	}
	for (unsigned int i=0; i<game->obstacle_quantity; i++) {
		game->obstacles[i].p = putOnCollisionMapAtRandomPosition(game, Obstacle);
		game->obstacles[i].c = randomObstacleCharacter(game);
	}
}

void writeLevelMapToCollisionMap(struct st_game *game) {
	// no map if null
	if (game->level_map.file == NULL) return;
	// start from 1 (ignore border 0/0)
	for (unsigned int y=1; y < game->ylen; y++) {
		for (unsigned int x=1; x<game->xlen; x++) {
			char *s = game->level_map.file[y + game->level_map.start_line];
			// if character is "#" --> set wall
			if (s[x] == '#') {
				struct st_pos p = {.x = x, .y = y};
				setCollisionMapCell(game, &p, Wall);
			}
		}
	}
}

void freeLevelMapFile(struct st_level_map *level_map) {
	if (level_map->file != NULL) {
		for (unsigned int i=0; i<level_map->file_array_len; i++) {
			free(level_map->file[i]);
		}
		free(level_map->file);
	}
}

void readLevelMapFile(struct st_game *game, char *file_name) {
	// read file
	FILE *file = fopen(file_name, "r");
	if (file == NULL) {
		fprintf(stdout, "error: level file \"%s\" not found.", file_name);
		exit(1);
	}
	const unsigned MAX_LENGTH = 256;
	char buffer[MAX_LENGTH];

	if (game->level_map.file != NULL) {
		freeLevelMapFile(&game->level_map);
	}
	game->level_map.player_starting_position.x = 0; // dummy value
	game->level_map.player_starting_position.y = 0; // dummy value
	game->level_map.file = NULL;
	game->level_map.file_name = file_name;
	game->level_map.file_line_cnt = 0;
	game->level_map.file_array_len = 0;
	// read line by line
	while (fgets(buffer, MAX_LENGTH, file)) {
		// remove trailing new line character
		buffer[strcspn(buffer, "\n")] = 0;
		int len = strlen(buffer)+1;
		char *str = malloc(sizeof(char *) * len);
		// snprintf will null terminate string
		snprintf(str, len, "%s", buffer);
		// write line by line to dynamically allocated array
		game->level_map.file = (char **) enlist((void **)game->level_map.file, str, game->level_map.file_line_cnt++, &game->level_map.file_array_len, 40, 1.5);
	}
	// find start and end of map data in file
	bool start = false;
	bool end = false;
	bool player = false;
	char **f = game->level_map.file;
	unsigned int xmax = 0;

	// prepare regex
	// regex https://en.wikibooks.org/wiki/Regular_Expressions/POSIX_Basic_Regular_Expressions
	regex_t regex_start;
	regex_t regex_end;
	assert_err(regcomp(&regex_start, "^" LEVEL_START "[:space:]*$", 0) == 0, "error with <start> regex");
	assert_err(regcomp(&regex_end, "^" LEVEL_END, 0) == 0, "error with <end> regex");

	for (unsigned int i=0; i < game->level_map.file_line_cnt; i++) {

		// count width of level map
		if ((start == true) && (xmax == 0)) {
			char *s = f[i];
			while (s[xmax] == '#') {
				xmax++;
			}
		}
		// search (regex) for "start" tag
		if ((start == false) && (regexec(&regex_start, f[i], 0, NULL, 0)==0)) {
			game->level_map.start_line = i+1;
			start = true;
			continue;
		}
		// search (regex) for "player" starting position tag
		if ((start == true) && (player == false)) {
			// string has to be null terminated for strchr to work!
			// string is null terminated because of snprintf
			char *p = strchr(f[i], LEVEL_PLAYER);
			if (p != NULL) {
				player = true;
				game->level_map.player_starting_position.x = (unsigned int) (p - f[i] - 1);
				game->level_map.player_starting_position.y = (unsigned int) (i - game->level_map.start_line - 1);
			}
		}
		// search (regex) for "end" tag
		if ((end == false) && (regexec(&regex_end, f[i], 0, NULL, 0)==0)) {
			game->level_map.end_line = i;
			end = true;
			break;
		}
	}

	// free regex
	regfree(&regex_start);
	regfree(&regex_end);

	// check if start, end and player found
	assert_err(start, "invalid level map file, cannot find <" LEVEL_START"> in file \"%s\"\n", file_name);
	assert_err(end,     "invalid level map file, cannot find <" LEVEL_END"> in file \"%s\"\n", file_name);
	//assert_err(player,   "could not find player starting position in level map file \"%s\"\n", file_name);

	// set level map boundaries
	game->level_map.xmax = xmax - 1;
	game->level_map.ymax = game->level_map.end_line - game->level_map.start_line - 1;

	fclose(file);
}

void initLevelMap(struct st_game *game) {

	// no new map requested?
	if ((!game->random_map) && (game->change_map == None))
		// ****************
		return;
		// ****************

	// are there maps?
	if (game->cli_options->argc > 0) {
		unsigned int i = 0;
		// random map requested?
		if (game->random_map) {
			if (game->cli_options->argc > 1) {
				do {
					// pick a new random map at random index
					i = rand() % (game->cli_options->argc);
				} while (i == game->level_map.index);
			}
			game->level_map.index = i;
		}
		// if not, inc/dec index
		else if (game->change_map == Next) {
			game->level_map.index += 1;
			// overflow?
			if (game->level_map.index >= game->cli_options->argc) {
				game->level_map.index = 0;
			}
			i = game->level_map.index;
		}
		else if (game->change_map == Previous) {
			// underflow?
			if (game->level_map.index <= 0) {
				game->level_map.index = game->cli_options->argc;
			}
			else {
				game->level_map.index--;
			}
			i = game->level_map.index;
		}
		char *map_path = game->cli_options->argz;
		// search for map at index i
		while (i>0) {
			// get next
			map_path = argz_next(game->cli_options->argz, game->cli_options->argz_len, map_path);
			i--;
		}
		// init map
		if (map_path != NULL) readLevelMapFile(game, map_path);
	}
	// change done
	game->change_map = None;
}


void newGame(struct st_game *game) {

	// load new level map if required
	initLevelMap(game);

	// init values
	game->cnt = 0;
	game->yay = 0;
	game->running = true;
	game->redraw_game = true;
	game->clear_screen = true;
	game->clear_status = true;
	game->zero_speed = false;
	game->portals_cnt = 0;

	// copy cli options
	game->skiptitle = game->cli_options->skiptitle;
	game->widemode = game->cli_options->widemode;
	game->unicorn = game->cli_options->unicorn;
	game->yummy = game->cli_options->yummy;
	game->portals_quantity = game->cli_options->portals;
	game->food_quantity = game->cli_options->food;
	game->obstacle_quantity = game->cli_options->obstacles;
	game->debug = game->cli_options->debug;
	game->sleep = game->cli_options->speed;
	game->growrate = game->cli_options->growrate;
	game->speedfactor = game->cli_options->speedfactor;

	// special modes
	if (game->unicorn) {
		game->widemode = true;
		game->yummy = true;
	} else if (game->yummy) {
		game->widemode = true;
	}

	game->xfact = game->widemode ? 2 : 1;

	// get terminal lines/columns
	// https://c-for-dummies.com/blog/?p=5730
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	game->terminal.lines = w.ws_row;

	// if wide, number of columns is divided by 2
	game->terminal.columns = w.ws_col/game->xfact;

	game->xoff = SPACING + 2;
	game->yoff = SPACING + 1;

	// no level map (default)
	game->xlen = game->terminal.columns - game->xoff * 2 + 1;
	game->ylen = game->terminal.lines - game->yoff * 2 + 1;

	// wide mode
	if (game->widemode) {
		game->xlen += 2;
		// if terminal width odd
		if (game->terminal.columns % 2 > 0) game->xlen++;
	} 
	// level map loaded --> check which is smaller: level or terminal
	if (game->level_map.file != NULL) {
		game->xlen = MIN(game->xlen, game->level_map.xmax);
		game->ylen = MIN(game->ylen, game->level_map.ymax);
	}

	// this has to be after setting the terminal x/y min/max
	// but before init of agents!
	initCollisionMap(game);

	// read the 4 corners of collision map
	// if there is a bug (out of bounds), debugger will see it
#ifdef DEBUG
	struct st_pos p;
	p.x = 1;
	p.y = 1;
	assert(!checkCollisionMap(game, &p));
	assert_err(1, "debug exit");
	assert(!checkOutOfBounds(game, &p));
	p.x = game->xlen-1;
	p.y = 1;
	assert(!checkOutOfBounds(game, &p));
	assert(!checkCollisionMap(game, &p));
	p.x = game->xlen-1;
	p.y = game->ylen-1;
	assert(!checkOutOfBounds(game, &p));
	assert(!checkCollisionMap(game, &p));
	p.x = 1;
	p.y = game->ylen-1;
	assert(!checkOutOfBounds(game, &p));
	assert(!checkCollisionMap(game, &p));
#endif

	// init agents (map, snake, obstacles, food, portals)
	writeLevelMapToCollisionMap(game);

	// no map or start out of bounds? set obstacles first
	if ((game->level_map.file == NULL) || (checkOutOfBounds(game, &game->level_map.player_starting_position))) {
		initObstacles(game);
		// sets also snake color
		initSnake(game);
	} 
	// set snake to predefined position, then put obstacles
	else {
		// sets also snake color
		initSnake(game);
		initObstacles(game);
	}
	initPortals(game, -1);
	initFood(game, -1);

	// pick individual colors
	do {
		game->border_color = randomColor();
	} while (game->border_color == game->snake->c.color);

	do {
		game->obstacle_color = randomColor();
	} while ((game->obstacle_color == game->snake->c.color) || (game->obstacle_color == game->border_color));

	// update of old xy only after init of new agents (which free memory based on old xy)
	game->xlen_old = game->xlen;
	game->ylen_old = game->ylen;
}

struct st_game *initGame(struct st_game *game) {

	// hide cursor
	printf(HIDE_CURSOR);
	set_conio_terminal_mode();
	// disable buffering for stdout
	// without this, printf is not working (when buffering is enabled for stdout)
	setbuf(stdout, NULL);

	// init rand
	srand(time(NULL));

	if (game == NULL)
		game = malloc(sizeof(struct st_game));

	// init pointers
	game->cmap = NULL;
	game->snake = NULL;
	game->portals = NULL;
	game->food = NULL;
	game->obstacles = NULL;

	// init level map (if necessary)
	// game is "empty" at this point
	game->level_map.file_name = NULL;
	game->level_map.file = NULL;
	game->level_map.index = game->cli_options->argc;
	game->level_map.file_line_cnt = 0;
	game->level_map.start_line = 0;
	game->level_map.end_line = 0;
	game->level_map.file_array_len = 0;
	game->level_map.player_starting_position.x = 0;
	game->level_map.player_starting_position.y = 0;
	game->level_map.xmax = 0;
	game->level_map.ymax = 0;
	game->random_map = false;
	game->change_map = Next;

	// init values
	newGame(game);

	// overwrite preset value
	game->running = false;

	// skip title screen?
	if (game->skiptitle) {
		game->state = Game;
		game->new_state = Game;
	} 
	// no --> show title screen
	else {
		game->state = Title;
		game->new_state = Title;
	}

	return game;
}

int kbhit() {
	struct timeval tv = { 0L, 0L };
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(0, &fds);
	return select(1, &fds, NULL, NULL, &tv) > 0;
}

int getch() {
	int r;
	unsigned char c;
	if ((r = read(0, &c, sizeof(c))) < 0) {
		// read failed
		return r;
	} else {
		// read successful
		return c;
	}
}

void printUnicorn() {
	// unicorn caracter "🦄" utf32 "1F984" https://www.compart.com/de/unicode/U+1F984
	printf("\U0001F984");
}

void printBorderCharacter(struct st_game *game) {
	// unicorn mode
	if (game->unicorn) {
		int i = rand() % (sizeof(hearts)/sizeof(hearts[0]));
		printf("%s", hearts[i]);
	} 
	// wide mode
	else if (game->widemode) {
		printf(WIDE_SNAKE_CHAR);
	} 
	// single char mode
	else {
		printf(SNAKE_CHAR);
	}
}

void draw(struct st_game *game) {

	// clear screen
	if (game->clear_screen) {
		clear;
		game->clear_screen = false;
	}

	// clear status line
	if (game->clear_status) {
		CUR(2,1);
		printf(ESC"2K"); // erase entire line
		game->clear_status = false;
	}


	// title screen
	if (game->state == Title) {
		int l = 1;
		CUR(2,l++);
		printf(RESET BOLD GREEN);
		printf(TITLE);
		l++;
		CUR(2,l++);
		printf(RESET GREEN);
		printf("Press <c> for credits and copyright");
		CUR(2,l++);
		printf("To show settings, launch game with \"--help\"");
		CUR(2,l++);
		printf("Press anytime <q> to quit and <?> for help");
		l++;
		l++;
		CUR(2,l++);
		printf(BOLD GREEN);
		printf("Press <space> to start a new game");
		// ************
		return;
		// ************
	} 

	// credits
	if (game->state == Credits) {
		int l = 1;
		CUR(2,l++);
		printf(RESET BOLD GREEN);
		printf(TITLE);
		printf(RESET GREEN);
		l++;
		CUR(2,l++);
		printf(COPYRIGHT1);
		CUR(2,l++);
		printf(COPYRIGHT2);
		CUR(2,l++);
		printf(COPYRIGHT3);
		CUR(2,l++);
		printf(COPYRIGHT4);
		l++;
		CUR(2,l++);
		printf("\"" BUILD "\" build and " ARCHITECTURE " architecture.");
		l++;
		l++;
		CUR(2,l++);
		printf(RESET BOLD GREEN);
		printf("Press <space> to return to title screen");
		// ************
		return;
		// ************
	} 

	// help
	if (game->state == Help) {
		int l = 1;
		CUR(2,l++);
		printf(RESET BOLD GREEN);
		printf(TITLE);
		l++;
		printf(RESET GREEN);
		CUR(2,l++);
		printf("Use arrow keys to control snake");
		CUR(2,l++);
		printf("Eat food (*) to grow snake");
		CUR(2,l++);
		printf("Enter portal to teleport to other random portal");
		CUR(2,l++);
		printf("Collision with snake, obstacles or border ends game");
		CUR(2,l++);
		printf("Unicorns like yummy things and dispise yucky things");
		l++;
		CUR(2,l++);
		printf("Press <?> to see this help");
		CUR(2,l++);
		printf("Press <space> to pause game");
		CUR(2,l++);
		printf("Press <n/p> to go to next/previous map");
		CUR(2,l++);
		printf("Press <r> to toggle random map selection");
		CUR(2,l++);
		printf("Press <esc> to abandon game and return to title screen");
		CUR(2,l++);
		printf("Press <q> to quit game");
		l++;
		CUR(2,l++);
		printf(RESET GREEN);
		printf("To show settings, launch game with \"--help\"");
		l++;
		CUR(2,l++);
		printf("See https://www.gitlab.com/oli79/snakes for source code,");
		CUR(2,l++);
		printf("bug reports and help");
		l++;
		CUR(2,l++);
		printf(RESET BOLD GREEN);
		printf("Press <space> to quit help");
		// ************
		return;
		// ************
	}

	// win screen - max score achieved!
	if (game->state == Win) {
		// status
		CUR(2,1);
		printf(BOLD GREEN "Max score reached! Congratulations, you beat the game! Press <space> to restart.");
		// ************
		return;
		// ************
	}

	// death screen - player dead
	if (game->state == Death) {
		// status
		CUR(2,1);
		printf(BOLD RED "GAME OVER! Press <space> to restart.");
		// ************
		return;
		// ************
	}

	// game pause
	if (game->state == Pause) {
		// status
		CUR(2,1);
		printf(BOLD BLUE "Game paused - press <space> to resume");
		// ************
		return;
		// ************
	}
	struct st_snake *s = game->snake;

	// debug status
	if (game->debug) {
		CUR(0,game->ylen+3);
		printf(BOLD GREEN);
		if (game->yay>0) {
			game->yay--;
			printf("YAY! ");
		}
		printf("cnt %ld delay %ldms", game->cnt, game->sleep);
		printf(" head %u %u:%u grow+%u tail %u %u:%u len %u", s->head, s->body[s->head].x, s->body[s->head].y, s->growing, s->tail, s->body[s->tail].x, s->body[s->tail].y, s->length);
		printf(" food %u:%u   ", game->food[0].p.x, game->food[0].p.y);
	} 
	// show status
	{
		CUR(0,1);
		printf(BOLD GREEN "score %u ", game->snake->length);
		// playing a level?
		if (game->level_map.file_name != NULL) {
			printf("\"%s\" [%i/%li]", game->level_map.file_name, game->level_map.index + 1, game->cli_options->argc);
		}
		if (game->random_map) {
			printf("(r)");
		} 
		else {
			printf("   ");
		}
	}


	// draw snake

	// snake tail
	// draw tail before snake (because otherwise, if head chases tail, the code below will overwrite head)
	// draw only tail if snake is not growing and not out of bounds
	if ((s->tail != s->head) && (s->growing == 0) && (!checkOutOfBounds(game, &(s->body[s->tail])))) {
		CUR2(s->body[s->tail].x, s->body[s->tail].y, game->xfact);
		if (game->widemode) {
			printf(RESET "  ");
		}
		else {
			printf(RESET " ");
		}
	}

	// snake head
	CUR2(s->body[s->head].x, s->body[s->head].y, game->xfact);
	if (game->unicorn) {
		printf(RESET "%s", s->c.color);
		printUnicorn();
	}
	else {
		// without reset, depending on what was printed during last printf, this character is printed in bold
		printf(RESET "%s%s", s->c.color, s->c.c);
	}

	// redraw snake
	if (game->redraw_game) {
		game->redraw_game = false;
		// snake
		unsigned int t = s->tail;
		printf("%s", s->c.color);
		for (unsigned int i=1; i<s->length; i++) {
			unsigned int j = (unsigned int) ((t+i) % SNAKE_BODY_MAX);
			// redraw snake
			CUR2(s->body[j].x, s->body[j].y, game->xfact);
			printf("%s", s->c.c);
		}

		// only redraw border if necessary (in unicorn mode, border is made of random characters)

		// border play field
		printf("%s", game->border_color);
		// top
		CUR2(0, 0, game->xfact);
		for (unsigned int i=0; i<=game->xlen; i++) {
			printBorderCharacter(game);
		}
		// bottom
		CUR2(0, game->ylen, game->xfact);
		for (unsigned int i=0; i<=game->xlen; i++) {
			printBorderCharacter(game);
		}
		// left
		for (unsigned int i=0; i<=game->ylen; i++) {
			CUR2(0, i, game->xfact);
			printBorderCharacter(game);
		}
		// right
		for (unsigned int i=0; i<=game->ylen; i++) {
			CUR2(game->xlen, i, game->xfact);
			printBorderCharacter(game);
		}

		// draw level map walls
		if (game->level_map.file != NULL) {
			for (unsigned int y=0; y < game->ylen; y++) {
				for (unsigned int x=0; x < game->xlen; x++) {
					char *s = game->level_map.file[y + game->level_map.start_line];
					// if character is "#" --> set wall
					if (s[x] == '#') {
						CUR2(x, y, game->xfact);
						printBorderCharacter(game);
					}
				}
			}
		}

	}

	// TODO draw border, walls, food and obstacles only if necessary
	// in reality, drawing is so fast, it does not matter at all
	if (true) {

		// draw portals
		if (game->portals_quantity > 0) {
			game->portals_cnt = (game->portals_cnt+1) % PORTAL_CNT_MAX;
			for (unsigned int i=0; i<game->portals_quantity; i++) {
				CUR2(game->portals[i].p.x, game->portals[i].p.y, game->xfact);
				if (game->portals_cnt == 0) {
					game->portals[i].c.color = randomPortalColor();
				}
				printf("%s%s", game->portals[i].c.color, game->portals[i].c.c);
			}
			// clear background color
			printf(RESET);
		}

		// draw food
		for (unsigned int i=0; i<game->food_quantity; i++) {
			CUR2(game->food[i].p.x, game->food[i].p.y, game->xfact);
			printf("%s%s", game->food[i].c.color, game->food[i].c.c);
		}

		// draw obstacles
		for (unsigned int i=0; i<game->obstacle_quantity; i++) {
			CUR2(game->obstacles[i].p.x, game->obstacles[i].p.y, game->xfact);
			printf("%s%s", game->obstacle_color, game->obstacles[i].c);
		}
	}

}


// reads game->new_state
// sets game->state
void nextState(struct st_game *game) {

	assert_err((game->snake->dir==Stop) || (game->snake->dir==Left) || (game->snake->dir==Right) || (game->snake->dir==Up) || (game->snake->dir==Down), "snake dir has invalid direction: %i", game->snake->dir);

	game->cnt++;

	// start new game
	if ((game->new_state == Game) && (game->state == Title)) {
		newGame(game);
		game->state = Game;
		game->redraw_game = true;
		game->clear_screen = true;
		// ************
		return;
		// ************
	}

	// continue game - back from pause
	if ((game->new_state == Game) && (game->state == Pause)) {
		game->state = Game;
		game->clear_screen = true;
		game->redraw_game = true;
		// ************
		return;
		// ************
	}

	// update state = new_state
	if (game->new_state != game->state) {
		game->state = game->new_state;
		game->clear_screen = true;
	}

	// return if no game running
	if (game->state != Game) {
		// ************
		return;
		// ************
	}

	// during game

	struct st_snake *s = game->snake;

	// move snake
	// ***********************

	// snake
	unsigned int old_x = s->body[s->head].x;
	unsigned int old_y = s->body[s->head].y;

	bool step = false;

	if (s->new_dir != Stop) {
		s->dir = s->new_dir;
		if (game->zero_speed) {
			s->new_dir = Stop;
			step = true;
		}
	}

	if ((!game->zero_speed) || (step)) {
		// move head in buffer
		s->head = (s->head+1) % SNAKE_BODY_MAX;

		// move tail in buffer
		if (s->growing > 0) {
			// decrease grow
			if (s->length < SNAKE_BODY_MAX-1) {
				s->growing--;
				s->length++;
			} else {
				// cannot grow anymore - game won
				game->new_state = Win;
				game->state = Win;
			}
		} else {
			// or move tail in buffer
			// remove from collision map (if not out of bounds)
			if (!checkOutOfBounds(game, &(game->snake->body[game->snake->tail]))) {
				setCollisionMapCell(game, &(game->snake->body[game->snake->tail]), Empty);
			}
			// move tail in buffer
			s->tail = (s->tail+1) % SNAKE_BODY_MAX;
		}

		// new head position
		s->body[s->head].x = old_x;
		s->body[s->head].y = old_y;
		switch (s->dir) {
			case Right:
				s->body[s->head].x++;
				break;
			case Left:
				s->body[s->head].x--;
				break;
			case Up:
				s->body[s->head].y--;
				break;
			case Down:
				s->body[s->head].y++;
				break;
			default:
				assert_err(s->dir != 0, "dir has no valid direction: %i", s->dir);
		}

		// first check if new snake (head) position is out of bounds
		if (checkOutOfBounds(game, &(game->snake->body[game->snake->head]))) {
			game->new_state = Death;
			game->state = Death;
			game->clear_status = true;
		} 
		// check if snake enters portal
		else if ((getCollisionMapCell(game, &(s->body[s->head])) >= Portals) && (getCollisionMapCell(game, &(s->body[s->head])) < Food)) {
			// teleport snake to other random portal
			unsigned int i = getCollisionMapCell(game, &(s->body[s->head])) - Portals;
			unsigned int new = 0;
			do {
				new = rand() % game->portals_quantity;
			} while (new == i);
			s->body[s->head] = game->portals[new].p;
		}
		// check if snake eats food
		else if (getCollisionMapCell(game, &(s->body[s->head])) >= Food) {
			// cheer
			game->yay += 5;
			// grow snake
			s->growing += game->growrate;
			// new food (at index)
			initFood(game, getCollisionMapCell(game, &(s->body[s->head])) - Food);
			// go faster
			game->sleep = game->sleep * SPEED_DIVIDEND / (SPEED_DIVIDEND + game->speedfactor);
			// collision map head position / ex-food position will be set with snake value further below
			// add new snake head position to collision map (overwrite food if applicable)
			setCollisionMapCell(game, &(game->snake->body[game->snake->head]), Snake);
		}
		// check if new snake (head) position has collision (has to be after food check,
		else if (checkCollisionMap(game, &(game->snake->body[game->snake->head]))) {
			game->new_state = Death;
			game->state = Death;
			game->clear_status = true;
		}
		// no collision
		else {
			// add new snake head position to collision map (overwrite food if applicable)
			setCollisionMapCell(game, &(game->snake->body[game->snake->head]), Snake);
		}
	}
	// rewrite portals to collision map (hack)
	for (unsigned int i=0; i<game->portals_quantity; i++) {
		setCollisionMapCell(game, &game->portals[i].p, Portals+i);
	}
}

// reads game->state
// sets game->new_state
void inputs(struct st_game *game) {

	// get key
	int key = 0;
	char c = 0;

	// return if no key
	if (!kbhit()) {
		// **************
		return;
		// **************
	}
	key = getch(); // consume character

	// return if key not valid
	if (key > 0) 
		c = (char) key;
	else {
		// **************
		return;
		// **************
	}

	// q for quit (anytime)
	if (c == 'q') {
		game->new_state = Quit;
		// **************
		return;
		// **************
	} 

	// ? for help (anytime)
	if ((c == 'h') || (c == '?')) {
		game->new_state = Help;
		game->clear_status = true;
		// **************
		return;
		// **************
	} 

	// quit --> no way back
	if (game->state == Quit) {
		// **************
		return;
		// **************
	}

	// title
	if (game->state == Title) {
		if (c == ' ') {
			game->new_state = Game;
			game->redraw_game = true;
			game->clear_status = true;
			// **************
			return;
			// **************
		}
		if (c == 'c') {
			game->new_state = Credits;
			game->clear_status = true;
			// **************
			return;
			// **************
		}
	}

	// credits
	if (game->state == Credits) {
		if ((c == ' ') || (c == 27)) {
			// if kbhit --> not esc but other key (arrow, alt+, etc.)
			if (!kbhit()) {
				game->clear_status = true;
				game->new_state = Title;
			}
			// **************
			return;
			// **************
		}
	}

	// win
	if (game->state == Win) {
		if ((c == ' ') || (c == 27)) {
			// kbhit --> not esc but other key (arrow, alt+, etc.)
			if (!kbhit()) {
				game->new_state = Title;
				game->clear_status = true;
			}
			// **************
			return;
			// **************
		}
	}

	// death or win
	if ((game->state == Win) || (game->state == Death)) {
		if ((c == ' ') || (c == 27)) {
			// kbhit --> not esc but other key (arrow, alt+, etc.)
			if (!kbhit()) {
				game->clear_status = true;
				game->new_state = Title;
			}
			// **************
			return;
			// **************
		}
	}

	// help
	if (game->state == Help) {
		if ((c == ' ') || (c == 27)) {
			// kbhit --> not esc but other key (arrow, alt+, etc.)
			if (!kbhit()) {
				if (game->running) {
					game->clear_screen = true;
					game->redraw_game = true;
					game->new_state = Game;
				} else {
					game->clear_screen = true;
					game->new_state = Title;
				}
			}
			// **************
			return;
			// **************
		}
	}

	// pause
	if (game->state == Pause) {
		if ((c == ' ') || (c == 27)) {
			// kbhit --> not esc but other key (arrow, alt+, etc.)
			if (!kbhit()) {
				game->redraw_game = true;
				game->new_state = Game;
			}
			// **************
			return;
			// **************
		}
	}

	// game
	if (game->state == Game) {
		// pressed an arrow key
		if (c == 27) {
			// single key? --> escape key
			if (!kbhit()) {
				game->clear_screen = true;
				game->new_state = Title;
				game->running = false;
			} 
			// more than one key? possiby arrow key
			else {
				getch(); // consume '['
				c = getch(); // get arrow
				switch (c) {
					case 'A':
						if (game->snake->dir != Down)
							game->snake->new_dir = Up;
						break;
					case 'B':
						if (game->snake->dir != Up)
							game->snake->new_dir = Down;
						break;
					case 'C':
						if (game->snake->dir != Left)
							game->snake->new_dir = Right;
						break;
					case 'D':
						if (game->snake->dir != Right)
							game->snake->new_dir = Left;
						break;
					default: 
						// ignore
						break;
				}
			}
		}
		// help
		else if (c == '?') {
			game->new_state = Help;
			// **************
			return;
			// **************
		}
		// toggle random maps
		else if (c == 'r') {
			// are there any maps?
			if (game->cli_options->argc > 0) {
				game->random_map = !game->random_map;
			}
			// **************
			return;
			// **************
		}
		// previous map / instant death
		else if (c == 'p') {
			// are there any maps?
			if (game->cli_options->argc > 0) {
				game->change_map = Previous;
				newGame(game);
				game->state = Game;
				game->redraw_game = true;
				game->clear_screen = true;
			}
			// **************
			return;
			// **************
		}
		// next map / instant death
		else if (c == 'n') {
			// are there any maps?
			if (game->cli_options->argc > 0) {
				game->change_map = Next;
				newGame(game);
				game->state = Game;
				game->redraw_game = true;
				game->clear_screen = true;
			}
			// **************
			return;
			// **************
		}
		// instant death (instant restart)
		else if ((c == 'D') || (c == 'U')) {
			newGame(game);
			game->state = Game;
			game->redraw_game = true;
			game->clear_screen = true;
			// **************
			return;
			// **************
		}
		// toggle zero speed
		else if ((c == '0') || (c == 'I')) {
			game->zero_speed = !game->zero_speed;
			// **************
			return;
			// **************
		}
		// spawn new food / grow snake
		else if (c == '*') {
			game->snake->growing += game->growrate;
			if (game->food_quantity > 1) {
				// replace random food
				initFood(game, rand() % game->food_quantity);
			} else {
				// replace one and only food
				initFood(game, 0);
			}
			// **************
			return;
			// **************
		}
		// decrease speed
		else if (c == '-') {
			// decrease speed
			int i = 3;
			// if speedfactor is 0, use 3 as default for inc/dec
			if (game->speedfactor != 0)
				i = game->speedfactor;
			game->sleep = game->sleep * (SPEED_DIVIDEND + i) / SPEED_DIVIDEND;
			// **************
			return;
			// **************
		}
		// increase speed
		else if (c == '+') {
			// increase speed
			int i = 3;
			// if speedfactor is 0, use 3 as default for inc/dec
			if (game->speedfactor != 0)
				i = game->speedfactor;
			game->sleep = game->sleep * SPEED_DIVIDEND / (SPEED_DIVIDEND + i);
			// **************
			return;
			// **************
		}
		// activate debug screen
		else if (c == 'd') {
			game->debug = !(game->debug);
			if (!game->debug) {
				game->clear_screen = true;
				game->redraw_game = true;
			}
			// **************
			return;
			// **************
		}
		// pause game
		else if (c == ' ') {
			game->new_state = Pause;
			// **************
			return;
			// **************
		}
	}
}

void play(struct st_game *game) {

	while (game->new_state != Quit) {

		draw(game);

		int sleep_cnt = 0;

		// poll user input
		do {
			inputs(game);
			if (game->state == Game) {
				// poll keyboard every 1ms during game play ...
				usleep(1000);
			} else {
				// ... and every 30ms during menu
				usleep(30*1000);
				sleep_cnt = game->sleep;
			}
			sleep_cnt++;
			// no need for sleep if in "stepping" (a.k.a. zero speed) mode
			if ((game->zero_speed) && (game->snake->new_dir != Stop)) {
				sleep_cnt = game->sleep;
			}
		} while ((game->new_state != Quit) && ( sleep_cnt < (game->sleep)));


		if (game->new_state != Quit) 
			nextState(game);
	} // Quit
}

// argp
//********************************************************************************

//const char *argp_program_bug_address = "<https://github.com/oli79/snakes>";
const char *argp_program_version = "version " VERSION;

// limits the arg to the range of min / max
int limit(char *arg, int min, int max) {
	int i = atoi(arg);
	if (i < min) return min;
	if (i > max) return max;
	return i;
}

static int parse_opt(int key, char *arg, struct argp_state *state)
{
	struct st_options *arguments = state->input;
	switch (key)
	{
		case 'c':
			arguments->speedfactor = 0;
			break;
		case 'd':
			arguments->debug = true;
			break;
		case 'f':
			arguments->food = limit(arg, 0, 200);
			break;
		case 'g':
			arguments->growrate = limit(arg, 0, 100);
			break;
		case 'o':
			arguments->obstacles = limit(arg, 0, 200);
			break;
		case 'p':
			{
				int i = limit(arg, 0, 100);
				if (i == 1) i = 2;
				arguments->portals = i;
			}
			break;
		case 's':
			arguments->speed = limit(arg, 0, 1000);
			break;
		case 'S':
			{
				// risk of divison by 0 if arg is 100 or -100
				int i = atoi(arg);
				if (i == 100) {
					i = 101;
				}
				else if (i == -100) {
					i = -101;
				}
				arguments->speedfactor = i;
			}
			break;
		case 't':  
			arguments->skiptitle = true;
			break;
		case 'w':
			arguments->widemode = true;
			break;
		case 'u':
			arguments->unicorn = true;
			break;
		case 'y':
			arguments->yummy = true;
			break;
		case ARGP_KEY_ARG:
			argz_add(&arguments->argz, &arguments->argz_len, arg);
			break;
		case ARGP_KEY_END:
			arguments->argc = argz_count(arguments->argz, arguments->argz_len);
	}
	return 0;
} 



// main
//********************************************************************************

void cleanup(struct st_game *game) {
	printf(SHOW_CURSOR);
	free(game->cli_options->argz);
	free(game->portals);
	free(game->food);
	free(game->snake);
	free(game->obstacles);
	freeCollisionMap(game);
	freeLevelMapFile(&game->level_map);
	free(game);
}


int main(int argc, char **argv)
{
	// setup argp option
	struct argp_option options[] =
	{
		{ "skiptitle", 't', NULL, 0, "Skip title screen and start game immediately", 0},
		{ "widemode", 'w', NULL, 0, "Wide mode (\"██\") (default is \"█\")", 0},
		{ "yummy", 'y', NULL, 0, "Yummy food (sets wide mode) (utf32 characters)", 0},
		{ "unicorns", 'u', NULL, 0, "Unicorns, hearts and yummy food (utf32 characters)", 0},
		{ "food", 'f', "NUM", 0, "Food quantity (default " STR(FOOD_QUANTITY) ")", 2},
		{ "obstacles", 'o', "NUM", 0, "Obstacle quantity (default " STR(OBSTACLES_QUANTITY) ")", 2},
		{ "portals", 'p', "NUM", 0, "Portal quantity (default " STR(PORTALS_QUANTITY) ")", 2},
		{ "growrate", 'g', "NUM", 0, "Grow rate per eaten food (default " STR(GROWRATE) ")", 3},
		{ "constspeed", 'c', NULL, 0, "Constant speed, i.e. don't increase speed when eating food (equivalent to \"-S 0\")", 4},
		{ "speed", 's', "NUM", 0, "Starting speed (lower=faster) (delay in ms per frame, default " STR(GAME_INIT_SLEEP) ")", 4},
		{ "debug", 'd', NULL, 0, "Enable debug status line", 5},
		{ "speedfactor", 'S', "NUM", 0, "Speed increase in % after food eaten (default " STR(SPEED_FACTOR) ")", 3},
		{ 0 }
	};

	// arg parse data struct
	struct argp argp = { options, parse_opt, LEVEL_HT, TITLE_HT "\v" COPYRIGHT, NULL, NULL, NULL };

	// setting default values for options
	struct st_options arguments =  {
		.skiptitle = false, 
		.speed = GAME_INIT_SLEEP, 
		.debug = false, 
		.portals = PORTALS_QUANTITY, 
		.food = FOOD_QUANTITY, 
		.obstacles = OBSTACLES_QUANTITY, 
		.growrate = GROWRATE, 
		.speedfactor = SPEED_FACTOR,
		.widemode = false,
		.unicorn = false,
		.yummy = false,
		.argc = 0,
		.argz = NULL,
		.argz_len = 0
	};

	// parse the options
	if (argp_parse(&argp, argc, argv, 0, 0, &arguments) == 0)
	{
		//********************************************************************************
		// program goes here
		//********************************************************************************
		struct st_game *game = malloc(sizeof(struct st_game));
		// handing over global options to game
		game->cli_options = &arguments;
		game = initGame(game);
		play(game);
		cleanup(game);
		//********************************************************************************
	}
	return 0; 
} 
