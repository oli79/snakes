
# snakes

[[_TOC_]]

## description

- simple terminal game made in C
- features are multiple food, random obstacles, level maps, teleporting portals, wide mode...
- tested for memory leaks and errors with valgrind
- tested on 32bit and 64bit architecture
- tested on x86 and ARM (raspberry pi)
- no dependencies other than GNU libs
- character placement done purely by ANSI escape sequences, i.e. no ncurses library is used
- for GNU/linux


## screenshots and gifs

### unicorn mode

- it is canon that unicorns cherish yummy things and dispise yucky things
- start game with ```snakes -u -f 30 -o 30```
- screenshot of kitty terminal, but gnome-terminal works too

![unicorn mode](img/unicorns1.png "unicorn mode")

### play nSnake level maps

- nSnake[^1] is a popular snake implementation
- see below how to install nSnake level maps
- to play the maps, start game with ```snakes path_to_levels/*```
- press ```n``` or ```p``` during game to select next or previous map
- press ```r``` to toggle random map selection

![nSnake level map](img/nsnake_level1.png "nSnake level map")
![nSnake level map](img/nsnake_level2.png "nSnake level map")
![nSnake level map](img/nsnake_level3.png "nSnake level map")

### wide mode and yummy food

- in wide mode, game tiles are two characters wide (looks better but needs more space)
- this allows for fancy unicode characters (emojis, like yummy food ```-y``` and unicorns ```-u```)
- level maps can also be played in wide mode
- start game with ```snakes -y -f 30 -o 30``` for 30 yummy food and 30 random obstacles
- screenshots of gnome-terminal

![wide mode](img/snakes_wide.png "wide mode")

![wide mode](img/snakes_wide2.png "wide mode")

### basic mode (no obstacles, single food, single character mode)

```snakes```

![snakes demo](img/snakes1.gif "snakes demo")

### loads of obstacles and food, including teleporting portals, single character mode

```snakes -t -f 30 -o 30 -p 10```

![snakes demo](img/snakes2.gif "snakes demo")

## installation

- requires ```gcc``` and ```git```!
- for the wide modes, use a terminal like gnome-terminal or kitty

### release build

clone, make and run the game

```
git clone https://gitlab.com/oli79/snakes
cd snakes
make release
bin/snakes --help
bin/snakes
```

### debug build

in order to compile with the compiler option ```-g``` omit the ```release``` key

```
git clone https://gitlab.com/oli79/snakes
cd snakes
make
bin/snakes
```

### download nSnake maps

- check out the levels in the folder ```mylevels```
 
- you can use ```make``` to install the nSnake level maps

```
make levels
```

- alternatively, either install nSnake with your package manager, or install it manually from github
- the levels can be found under ```nSnake/levels``` or in ```~/.local/bin/nsnake/levels```

```
git clone https://github.com/alexdantas/nSnake nSnake
```
 


## cli interface

plenty of game parameters can be set over the cli

```
snakes --help
```

try one of these options:

- plenty of everything
```
snakes -dt -f 30 -g 10 -s 102 -o 50
```

- wide mode
```
snakes -w
```

- yummy food
```
snakes -y -t -f 30 -o 30
```

- unicorn mode
```
snakes -u
```

## in game controls

### snake

- ```arrows``` change snake direction
- ```h``` or ```?``` show help screen
- ```p``` pause game
- ```q``` quit to cli
- ```n``` select next level map (start game with ```snakes levels/*```)
- ```p``` select previous level map
- ```r``` to toggle random map selection


### cheats 

- ```-``` decrease speed (increase delay per frame)
- ```+``` increase speed (decrease delay per frame)
- ```*``` spawn new food and increase length of snake
- ```d``` show debug info screen
- ```D``` instant death and game restart
- ```0``` toggle zero speed mode (cursor mode)
- during zero speed mode, use arrows to navigate
 

## future features (maybe)

- new keyboard / video handling implementing multi-threading
- new cli option: make snake length at game start a cli parameter
- add a software controlled adversary snake (player vs cpu)


## credits

### unicode character table

https://www.compart.com/de/unicode


https://www.compart.com/de/unicode/block/U+1F300

### argp

i am using the argp libray and there is a great tutorial online:

https://nongnu.askapache.com/argpbook/step-by-step-into-argp.pdf


## more snake implementations

### Nir Lichtman

I know this is no boot sector game, but this video was my inspiration to start this project

https://www.youtube.com/watch?v=tHO5x_Ha6pE


### nSnake
 
shout out to nSnake[^1], a popular snake implementation with level maps

### nibbles

anybody remembers nibbles on qbasic?

https://en.wikipedia.org/wiki/Nibbles_(video_game)

try it out online:

https://www.retrogames.cz/play_1620-DOS.php


## about me...

- i am looking for a position as a software developer in CH in the BE/SO area, available from august 2024
- i prefer to work on a GNU/linux host system (i.e. pc that runs linux)


## project email address

contact-project+oli79-snakes-55541100-issue-@incoming.gitlab.com

## footnotes

[^1]: nSnake github page: https://github.com/alexdantas/nSnake

